import ProductsList from "../components/products/ProductsList";

function Home(props) {
  return <ProductsList {...props} />;
}
export default Home;
