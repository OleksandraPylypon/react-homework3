import React from "react";
import "./header.scss";
import { Link } from "react-router-dom";

function Header({ cartCount, favoritesCount }) {
  return (
    <header className="header">
      <nav>
        <ul className="nav-menu">
          <li className="nav-element">
            <Link className="nav-link" to="/">
              Home
            </Link>
          </li>
          <li className="nav-element">
            <Link className="nav-link" to="/cart">
              Cart
            </Link>
          </li>
          <li className="nav-element">
            <Link className="nav-link" to="/favorite">
              Favorite
            </Link>
          </li>
        </ul>
      </nav>
      <div className="icons-container">
        <img className="star-icon" src="/images/star.svg" alt="Star Icon" />
        <div className="star-count">{favoritesCount}</div>
        <img className="cart-icon" src="/images/cart.svg" alt="Cart Icon" />
        <div className="cart-count">{cartCount}</div>
      </div>
    </header>
  );
}

export default Header;
