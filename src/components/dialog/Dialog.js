import React from "react";
import styles from "./Dialog.module.scss";

const Dialog = ({ header, closeButton, text, actions, onClose }) => {
  return (
    <div className={styles["dialog-overlay"]} onClick={onClose}>
      <div className={styles.dialog}>
        <div className={styles["dialog-header-container"]}>
          <h2 className={styles["dialog-header"]}>{header}</h2>
          {closeButton && (
            <span className={styles["close-button"]} onClick={onClose}>
              &times;
            </span>
          )}
        </div>
        <div className={styles["dialog-content"]}>{text}</div>
        <div className={styles["dialog-actions"]}>{actions}</div>
      </div>
    </div>
  );
};

export default Dialog;
