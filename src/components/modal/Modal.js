import React from "react";
import styles from "./Modal.module.scss";

const Modal = ({ text }) => {
  return (
    <div className={styles["modal-overlay"]}>
      <div className={styles.modal}>
        <div className={styles["modal-content"]}>{text}</div>
      </div>
    </div>
  );
};

export default Modal;
