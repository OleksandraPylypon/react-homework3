import React, { useState, useEffect } from "react";
import ProductsCard from "./productsCard";
import "./Products.scss";
import { useLocation } from "react-router-dom";

function ProductsList({ favoriteProducts, cartProducts, ...props }) {
  const [products, setProducts] = useState([]);
  const location = useLocation();

  useEffect(() => {
    if (location.pathname === "/favorite") {
      setProducts((currentProducts) =>
        currentProducts.filter((product) =>
          favoriteProducts.includes(product.name)
        )
      );
    }
  }, [favoriteProducts]);

  useEffect(() => {
    if (location.pathname === "/cart") {
      setProducts((currentProducts) =>
        currentProducts.filter((product) => cartProducts.includes(product.name))
      );
    }
  }, [cartProducts]);

  useEffect(() => {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => {
        

        if (location.pathname === "/favorite") {
          setProducts(
            data
              .filter((product) => favoriteProducts.includes(product.name))
              .map((product) => ({
                ...product,
                isFavorite: true,
                isAddedToCart: cartProducts.includes(product.name),
              }))
          );
          return;
        }

        if (location.pathname === "/cart") {
          setProducts(
            data
              .filter((product) => cartProducts.includes(product.name))
              .map((product) => ({
                ...product,
                isFavorite: favoriteProducts.includes(product.name),
                isAddedToCart: true,
              }))
          );
          return;
        }

        setProducts(
          data.map((product) => ({
            ...product,
            isFavorite: favoriteProducts.includes(product.name),
            isAddedToCart: cartProducts.includes(product.name),
          }))
        );
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  if (!products.length) {
    return <div className="not-found"> Products not found!</div>;
  }

  return (
    <div className="products-list">
      {products.map((product, index) => (
        <ProductsCard
          key={index}
          imagePath={product.imagePath}
          name={product.name}
          articleNumber={product.articleNumber}
          color={product.color}
          price={product.price}
          isFavorite={product.isFavorite}
          isAddedToCart={product.isAddedToCart}
          {...props}
        />
      ))}
    </div>
  );
}

export default ProductsList;
