import React, { useState } from "react";
import Modal from "../modal/Modal";
import "./Products.scss";
import { useLocation } from "react-router-dom";
import Dialog from "../dialog/Dialog";
import "../dialog/Dialog.module.scss";

function ProductsCard(props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isFavorite, setIsFavorite] = useState(props.isFavorite);
  const [isAddedToCart, setIsAddedToCart] = useState(props.isAddedToCart);
  const location = useLocation();
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const closeDialog = () => {
    setIsDialogOpen(false);
  };

  const openDialog = () => {
    setIsDialogOpen(true);
  };

  const confirmDialog = () => {
    props.handleChangeCartProducts("remove", props.name);
    closeDialog();
  };

  const openModal = () => {
    setIsModalOpen(true);

    props.handleChangeCartProducts(
      isAddedToCart ? "remove" : "add",
      props.name
    );

    setIsAddedToCart(!isAddedToCart);

    setTimeout(() => {
      setIsModalOpen(false);
    }, 1000);
  };

  const toggleFavorite = () => {
    props.handleChangeFavoriteProducts(
      isFavorite ? "remove" : "add",
      props.name
    );

    setIsFavorite(!isFavorite);
  };

  return (
    <div className="products-card">
      <img
        className={`icon-star ${isFavorite ? "favorite" : ""}`}
        src={isFavorite ? "/images/star-yellow.svg" : "/images/starblack.svg"}
        alt="star-icon"
        onClick={toggleFavorite}
      />

      {location.pathname === "/cart" && (
        <img
          className="icon-close"
          src="/images/close.svg"
          onClick={openDialog}
        />
      )}
      <img className="products-image" src={props.imagePath} alt="" />
      <p className="products-articleNumber">Артикул: {props.articleNumber}</p>
      <p className="products-name">{props.name}</p>
      <p className="products-price">{props.price}</p>
      <p className="products-color">Color: {props.color}</p>

      {location.pathname !== "/cart" && (
        <button
          className={`products-button ${isAddedToCart && "active"}`}
          onClick={openModal}
        >
          {isAddedToCart ? "Added" : "Add to cart"}
        </button>
      )}

      {isModalOpen && (
        <Modal
          className="modal"
          text={`You have added ${props.name} to your cart.`}
        />
      )}

      {isDialogOpen && (
        <Dialog
          header="Do you want to delete this product from the cart?"
          closeButton={true}
          text={<>Are you sure you want to delete it?</>}
          actions={
            <>
              <button className="custom-button" onClick={confirmDialog}>
                OK
              </button>
              <button className="custom-button" onClick={closeDialog}>
                Cancel
              </button>
            </>
          }
          onClose={closeDialog}
        />
      )}
    </div>
  );
}

export default ProductsCard;
