import React, { useState } from "react";
import ProductsList from "./components/products/ProductsList";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";
import Header from "./components/header/header";
import useLocalStorage from "./hooks/useLocalStorage";
import "./styles.scss";

function App() {
  const [cartProducts, setCartProducts] = useLocalStorage("cart-products", []);
  const [favoriteProducts, setFavoriteProducts] = useLocalStorage(
    "favorite-products",
    []
  );

  function handleChangeCartProducts(type, name) {
    switch (type) {
      case "add": {
        setCartProducts([...cartProducts, name]);
        break;
      }
      case "remove": {
        setCartProducts(
          cartProducts.filter((cartProduct) => cartProduct !== name)
        );
        break;
      }
    }
  }

  function handleChangeFavoriteProducts(type, name) {
    switch (type) {
      case "add": {
        setFavoriteProducts([...favoriteProducts, name]);
        break;
      }
      case "remove": {
        setFavoriteProducts(
          favoriteProducts.filter((favoriteProduct) => favoriteProduct !== name)
        );
        break;
      }
    }
  }

  return (
    <div className="App">
      <Header
        cartCount={cartProducts.length}
        favoritesCount={favoriteProducts.length}
      />

      <Routes>
        <Route
          path="/"
          element={
            <Home
              {...{
                cartProducts,
                favoriteProducts,
                handleChangeCartProducts,
                handleChangeFavoriteProducts,
              }}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              {...{
                cartProducts,
                favoriteProducts,
                handleChangeCartProducts,
                handleChangeFavoriteProducts,
              }}
            />
          }
        />
        <Route
          path="/favorite"
          element={
            <Favorite
              {...{
                cartProducts,
                favoriteProducts,
                handleChangeCartProducts,
                handleChangeFavoriteProducts,
              }}
            />
          }
        />
      </Routes>
    </div>
  );
}

export default App;
